// console.log("Hello, World!");

// Array Methods

/*
	-Javascript has a built-in function and methods for arrays. This allows us to manipulate and access array items.
	-Array can be either mutated or iterated.
		-Array "mutaions seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/

// mutator Methods
// Functions that mutate ir change an array.
// These manupulates the original array performing various task such as adding and removing elements.

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit",];

// push()
/*
	-Adds an element in the end of an array AND returns the new array's length.
	- Syntax:
		arrayName.push(newElement);
*/
console.log("Current Array: ");
console.log(fruits);
// fruits[fruits.lenght]= "Mango"
let fruitsLength = fruits.push("Mango");// returns the new length of the array once we added an element.
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

// Add multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop ()
/*
	- Remove the last element in an array AND returns the removed element.
	- Syntax:
		arrayName.pop();

*/
let removedFruit = fruits.pop();
console.log(removedFruit); //returns remove element
console.log("Mutated array from pop method:");
console.log(fruits);

// unshift()
/*
	- Add one or more elements at the beginning of an array.
	- Syntax:
		arrayName.unshift("elementA");
		arrayName.unshift("elementA", "elementB");
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:");
console.log(fruits);

//  shift()
/*
	- remove an element at the beginning of an array AND returns the removed element.
	- Syntax
		arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);

// splice()
/*
	-simultanously removes elements from a specified index number and adds elements.
	-Syntax: 
		arrayName.splice(startingIndex, deletecount, elementsToBeAdded)
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method:");
console.log(fruits);

// sort()
/*
	- Rearanges the array elements in alphanumeric order.
	- Syntax:
		- arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);

// reverse();
/*
	- Reverse the order of the array elements
	- Syntax:
		arrayname.reverse();
*/

fruits.reverse();
console.log("Mutated array from revers method:");
console.log(fruits);

// Non-mutator methods
/*
	- Non-mutator methods are functions that do not modify orchange an array after they're created.
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
/*
	-Returns the index of the first matching element found in an array.
	- If no match was found, the result will be -1.
	- The search process will be done from the first element proceeding to the last element.
	- Sytnax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/
let firstIndex = countries.indexOf("PH");
// let firstIndex = countries.indexOf("PH", 2); // with indexStart
console.log("Result of indexOf method: " +firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " +invalidCountry);

// lastIndexOf()
/*
	-Returns the index number of the last matching element found in an array.
	- The search from process will be done from the last element proceeding to the forst element.
	- Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue,fromIndex/EndingIndex);

*/
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf Method: " + lastIndex);

// Getting theindex of an array from a specific index number.
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf Method: " + lastIndexStart);

// Slice()
/*
	- Portions/slices element from an array AND returns a new array.
	- Syntax: 
		arrayName.slice(startingIndex); //until the last element of the array.
		arrayName.slice(startingIndex, endingIndex);
*/

	console.log("Original countries array:");
	console.log(countries)

// Slicing off elements from specified index to the last element.
	let sliceArrayA = countries.slice(2);
	console.log("Result from slice Method: ");
	console.log(sliceArrayA);

// Slicing of element from specified index to another index. But the specified last index is not included in the return.
	let sliceArrayB = countries.slice(2,5); //2 -> 4
	console.log("Result from slice Method: ");
	console.log(sliceArrayB);

// Slicing off elements starting from the last element of an array.
	let sliceArrayC = countries.slice(-3);
	console.log("Result from slice Method: ");
	console.log(sliceArrayC);

// toString()
/*
	- Returns an array as a string, seperated by commas.
	- Syntax: 
		arrayName.toString();
*/

	let stringArray = countries.toString();
	console.log("Result from toString Method: ");
	console.log(stringArray);
	console.log(typeof stringArray); // to check of the array is converted to string.

// concat()

/*
	- Combines two array AND returns the combined result.
	- Syntax
		arrayA.concat(arrayB);
		arrayA.concat(element);
*/

let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breath sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat Method: ");
console.log(tasks);

// Combining multiple arrays
console.log("Result from concat Method: ");
let alltasks = tasksArrayA.concat(tasksArrayB,tasksArrayC);
console.log(alltasks);

// Combine arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat Method: ");
console.log(combinedTasks);

// join()
/*
	- REturns an array as a string seperated by spcecified separator string.
	- Sytnax 
		arrayName.join("separatorString");
*/

let users = ["John", "Jane", "Joe", "Juan"];

console.log(users.join()); // default
console.log(users.join(' '));
console.log(users.join(" - "));

// Iteration Methods
/*
	- Iteration methods are loops designed to perform repetetive tasks on array.
	- It loops over all items/elements in an array.
*/

// forEach()
/*
	-Similar to a for loop that iterates on each array element.
	- For each item in the array, the anonymouis function passed in the forEach()method will be run.
	- The anonymous function is able to recieved the current item being iterated or loop over.
	- It's common practice to use the singular form of the array content for parameter names used in array loops.
	- forEach() does not return  anything.
	-Syntax:
		arrayName.forEach(function(indivElement){
			// statement/code block
		})
*/
	/*
		for(let i = 0; i<alltask.length; i++){
			console.log(allTask[i]);
		}
	*/

// using forEach with conditional statement.
let filteredTasks = []; //store in this variables all the tasks name with characters freater than 10.

				//anonymous function "parameter" represents each element of the array to be iterated.
alltasks.forEach(function(task){
	// It is a good practice to print the current element in the array console when working with array methods to have idea of what information ic being worked on for each iteration.
	// console.log(task);

	// if the element/strings length is greater than 10 characters.
	if(task.length > 10){
		// 

		// Add element to the filteredTasks array.
		filteredTasks.push(task);
	}
})

console.log("Result from filtered tasks: ");
console.log(filteredTasks);

// map()
/*
	- Iterates on each element and reaturns new array with different values depending on the result of the function's operation.
	- This is useful for performing tasks where mutating or changing the elements required.

	- Syntax
		let/const resultArray = arrayName.map(function(indivElement));
*/

let numbers = [1, 2, 3, 4, 5];

//return squared values of each element.
let numbersMap = numbers.map(function(number){
	return number*number;
})

console.log("Original Array: ")
console.log(numbers);// Original is not affected by map().

console.log("Result of the map method: ");
console.log(numbersMap);

// map() vd forEach ()

let numberForEach = numbers.forEach(function(number){
	return number*number;
})

console.log(numberForEach); //undefined
// forEach(), loops over all items in the array as  does map, but forEach() does no return a new array.

// every()
/*
	- check if all elements in an array meets the given condition.
	- This is useful for validating data stored in arrays especialy whendealing eith large amounts of data.
	- Returns a true value if all elements meet the consition, false otherwise.
	- Syntax:
		let/const = rsultArray = arayName.every(function(individual element{
		return expression/consition
		}))
*/

numbers = [5, 7, 9, 8, 5];	
	
let allValid = numbers.every(function(number){
	return(number < 3);
})

console.log("Result of every method: ");
console.log(allValid);

// some()
/*
	- Check if atleast 1 element in the array meets the given condition.
	- Returns a true value if 1 element meet the condition and false otherwise.
	- Syntax:
		let/const resultArray = arrayName.some(function(indivElement){
	return expression/condition;
		})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})

console.log("Result of some method: ");
console.log(someValid);

// Combining the returned results from ht every/some method may be used in other statements to perform consecutive result.
if(someValid){
	console.log("Some numbers in the array are greater than 2.");
}

// filter()
/*
	- return in new array thah contains elements which meets the given condition.
	- Return an empty array if no elements were found.
	- Useful for filtering array elements 
	- Syntax: 
		- let/const resultArray = arrayName.filter(function(indivElement){
			return expression/condition:
		})
*/

numbers = [3, 5, 3, 4, 5];

let filterValid = numbers.filter(function(number){
	return (number < 3);
});
console.log("Result of filter method: ");
console.log(filterValid);

//no elements found
let nothingFound = numbers.filter(function(number){
	return number == 0;
})
console.log("Result of filter method: ");
console.log(nothingFound);


// shorten the syntax

filteredTasks = alltasks.filter(function(task){
	return (task.length > 10);
})

console.log("Result of filter method: ");
console.log(filteredTasks);


//  includes()
/*
	- Checks if the argument passed can be found in the array.
	- it returns a boolean which can be saved in a variable.
	- Syntax:
		let/const variableName = arrayName.include(<argumentToFind>);
*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound = products.includes("Mouse");
console.log("Result of includes method: ");
console.log(productFound); //returns true

let productNotFound = products.includes("Headset");
console.log("Result of includes method: ");
console.log(productNotFound); //returns false

// Method Chaining
	// The result of the inner method is used in the outer method until all "chained" methods have been resolved.

	let filteredProducts = products.filter(function(product){
				//keyboard
		return product.toLowerCase().includes('a');
	})

	console.log("Result of chained method: ");
	console.log(filteredProducts); 

// reduce()
/*
	- Evaluates elements from left and right and returns or reduces the array into a single value.
	- Syntax
		let/const resultvariable = arrayName.reduce(function(accumulator, currentValue){
		return expression/operation
		});
			- accumulator parameter stores the result for every loop.
			- currentValue parameter refers to the current/next element in the array that is evaluated in each iteration of the loop.
*/

let i = 0; 

numbers = [1, 2, 3, 4, 5];

let reduceArray = numbers.reduce(function(acc, cur){
	console.warn("current iteration" + ++i);
	console.log("accumulator: " +acc);
	console.log("current value: "+cur);

	// The operation or expression to reduce the array into a single value.
	return acc + cur;
})
/*
	Hoe the "reduce" method works:
	1. the first/result element in the array is stored in the "acc" parameter.
	2. the second/next element in the array is stored in the "cur" parameter.
	3. an operation is performed on two elements.
	4. 
*/

console	.log("Result of the reduce method; "+reduceArray);